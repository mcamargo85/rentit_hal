package com.example.demo.inventory.application.service;

import com.example.demo.inventory.application.dto.Plant;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.rest.InventoryRestController;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class PlantInventoryEntryAssembler
        extends RepresentationModelAssemblerSupport<PlantInventoryEntry, Plant> {

    public PlantInventoryEntryAssembler() {
        super(InventoryRestController.class, Plant.class);
    }

    @Override
    public Plant toModel(PlantInventoryEntry plant) {
        Plant dto = createModelWithId(plant.getId(), plant);
        dto.set_id(plant.getId());
        dto.setName(plant.getName());
        dto.setDescription(plant.getDescription());
        dto.setPrice(plant.getPrice());
        return dto;
    }
}
