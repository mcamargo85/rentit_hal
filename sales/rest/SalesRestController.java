package com.example.demo.sales.rest;

import com.example.demo.common.application.exception.PlantNotFoundException;
import com.example.demo.inventory.application.dto.Plant;
import com.example.demo.inventory.application.service.InventoryService;
import com.example.demo.sales.application.dto.POExtensionDTO;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import com.example.demo.sales.application.service.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

@RestController
@RequestMapping("/api/sales")
@CrossOrigin
public class SalesRestController {
    @Autowired
    InventoryService inventoryService;

    @Autowired
    SalesService salesService;

    @GetMapping("/plants")
    public CollectionModel<Plant> findAvailablePlants(
            @RequestParam(name = "name") String plantName,
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate
    ) {
        return inventoryService.findAvailablePlants(plantName.toLowerCase(), startDate, endDate);
    }

    @GetMapping("/orders/{id}")
    @ResponseStatus(HttpStatus.OK)
    public PurchaseOrderDTO fetchPurchaseOrder(@PathVariable("id") Long id) {
        return salesService.findPO(id);
    }

    @GetMapping("/orders")
    public CollectionModel<PurchaseOrderDTO> findOrders()
    {
        return salesService.findPurchaseOrders();
    }

    @PostMapping("/orders")
    public ResponseEntity<?> createPurchaseOrder(@RequestBody PurchaseOrderDTO partialPODTO) throws Exception {
         PurchaseOrderDTO newlyCreatePODTO = salesService.createPO(partialPODTO);
         HttpHeaders headers = new HttpHeaders();
         headers.setLocation(newlyCreatePODTO.getRequiredLink(IanaLinkRelations.SELF).toUri());

         return new ResponseEntity<>( newlyCreatePODTO, headers, HttpStatus.CREATED);
//         return new ResponseEntity<>(
//                 new EntityModel<>(newlyCreatePODTO,
//                        linkTo(methodOn(SalesRestController.class).fetchPurchaseOrder(partialPODTO.get_id()))
//                                .withSelfRel(),
//                        linkTo(methodOn(SalesRestController.class).retrievePurchaseOrderExtensions(partialPODTO.get_id()))
//                         .withRel("extend")),
//                         headers, HttpStatus.CREATED);

    }

    @PostMapping("/orders/{id}/accept")
    public PurchaseOrderDTO acceptPurchaseOrder(@PathVariable Long id) throws Exception {
        try {
            return salesService.acceptPO(id);
        } catch (Exception ex) {
            // Add code to Handle Exception (Change return null with the solution)
            return null;
        }
    }

    @DeleteMapping("/orders/{id}/reject")
    public PurchaseOrderDTO rejectPurchaseOrder(@PathVariable Long id){
        try {
            return salesService.rejectPO(id);
        } catch (Exception ex) {
            // Add code to Handle Exception (Change return null with the solution)
            return null;
        }
    }

    @GetMapping("/orders/{id}/extensions")
    public CollectionModel<EntityModel<POExtensionDTO>> retrievePurchaseOrderExtensions(@PathVariable("id") Long id) {
        List<EntityModel<POExtensionDTO>> result = new ArrayList<>();
        POExtensionDTO extension = new POExtensionDTO();
        extension.setEndDate(LocalDate.now().plusWeeks(1));

        result.add(new EntityModel<>(extension));
        return new CollectionModel<>(result,
                linkTo(methodOn(SalesRestController.class).retrievePurchaseOrderExtensions(id))
                        .withSelfRel()
                        .andAffordance(afford(methodOn(SalesRestController.class).requestPurchaseOrderExtension(null, id))));
    }

    @PostMapping("/orders/{id}/extensions")
    public EntityModel<?> requestPurchaseOrderExtension(@RequestBody POExtensionDTO extension, @PathVariable("id") Long id) {
        return null;
    }

    @ExceptionHandler(PlantNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handPlantNotFoundException(PlantNotFoundException ex) {
    }
}